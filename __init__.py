import st3m.run, random

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context

import leds


class Devlol(Application):
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        leds.set_all_rgb(0, 0, 255)
        leds.set_slew_rate(1)
        leds.set_auto_update(1)
        leds.update()

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # print /dev/lol logo
        ctx.image(
            self.app_ctx.bundle_path + "/devlol-logo.png",
            -100,
            -100,
            200,
            200,
        )


if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Devlol(ApplicationContext()))
